extends Control




func _on_Yes_pressed():
	PILOT.delete_pilot_file( get_parent().selected_serial )
	get_parent().populate_pilots_list() # Repopulate list to clear out the dead pilot
	var notify = GLOBAL.notify( "Pilot file deleted!" )
	yield( notify, "dismissed" )
	hide()


func _on_No_pressed():
	hide()
