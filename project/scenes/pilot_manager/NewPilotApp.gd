extends Control

onready var name_edit = $Panel/Box/Name/Edit
onready var age_edit = $Panel/Box/Age/Edit

onready var submit_button = $Panel/Box/Submit

onready var serial_panel = $Panel/Box/Serial
onready var serial_label = $Panel/Box/Serial/Serial

var generated_serial = ""

func clear_form():
	self.generated_serial = ""
	name_edit.text = ""
	age_edit.value = age_edit.min_value
	$Panel/Box/Serial.hide()

func _ready():

	pass




func _on_Cancel_pressed():
	clear_form()
	hide()


func _on_Edit_text_changed(new_text):
	submit_button.disabled = new_text == ""


func _on_Submit_pressed():
	prints("Submitting paperwork for", name_edit.text)
	
	self.generated_serial = PILOT.generate_unique_serial()
	
	submit_button.disabled = true
	name_edit.editable = false
	age_edit.editable = false
	
	serial_label.text = self.generated_serial
	serial_panel.show()
	
	
	


func _on_OK_pressed():
	PILOT.create_pilot( self.generated_serial, name_edit.text, age_edit.value )
	
	var wait_for = GLOBAL.notify("A new pilot has been added to the rosters!","Indeed!")
	yield(wait_for,"dismissed")
	get_parent().populate_pilots_list()
	clear_form()
	hide()
	