extends Control

onready var pilots_list = $PilotPanel/Box/Left/Panel/List/Box

onready var edit_button = $PilotPanel/Box/Right/Choices/Edit
onready var delete_button = $PilotPanel/Box/Right/Choices/Delete
onready var play_button = $PilotPanel/Box/Left/Choices/Play

onready var data_panel = $PilotPanel/Box/Right/Data


var selected_serial setget _set_selected_serial




func clear_pilots_list():
	for node in pilots_list.get_children():
		node.queue_free()



func populate_pilots_list():
	# Flush the list
	clear_pilots_list()
	# Get data for the new list
	var list = PILOT.get_pilots_list()
	# Set up a button group for the pilotpanels
	var b_group = ButtonGroup.new()
	# Generate panels for each pilot
	for s in list:
		var data = PILOT.get_pilot_file( s )
		var new_panel = preload("res://scenes/pilot_manager/PilotPanel.tscn").instance()
		new_panel.connect("toggled", self, "_on_PilotPanel_toggled", [s] )
		new_panel.group = b_group
		new_panel.get_node("Box/Info/Serial").text = s
		new_panel.get_node("Box/Info/Name").text = data.name
		
		pilots_list.add_child(new_panel)










func _ready():
	populate_pilots_list()
	$Version.text = GLOBAL.get_name_and_version()


# Signal callbacks from RightPanel choice buttons
func _on_New_pressed():
	$NewPilotApp.show()

func _on_Edit_pressed():
	pass # replace with function body

func _on_Delete_pressed():
	$ConfirmDelete.show()


func _on_PilotPanel_toggled( toggled, serial ):
	if !toggled:
		return
	self.selected_serial = serial
	data_panel.show_pilot_data(PILOT.get_pilot_file( serial ))




func _set_selected_serial( what ):
	selected_serial = what
	
	var disable = selected_serial == null
	edit_button.disabled = disable
	delete_button.disabled = disable
	play_button.disabled = disable
	
#	var data = PILOT.get_pilot_file( selected_serial )






func _on_Play_pressed():
	PILOT.load_pilot( self.selected_serial )
#	yield( PILOT, "loaded_pilot_file" )
	get_parent().scene = GLOBAL.FLIGHT_SIM_SCENE






