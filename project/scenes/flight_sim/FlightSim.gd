extends Node2D

signal report_escape_velocity( to_velocity )

signal player_escapes()

signal player_begins_escape()
signal player_ends_escape()

var player_ship

var escaping = false

func spawn_player_ship( at_transform=null ):
	player_ship = preload("res://scenes/flight_sim/vehicle/Vehicle.tscn").instance()
	
	add_child(player_ship)
	
	# Link player ship to HUD
	player_ship.connect_to_ui( $ShipUI )
	# Link velocity monitoring to self
	player_ship.connect( "report_linear_velocity", self, "_on_player_ship_report_linear_velocity" )
	# Place player ship at a point/heading in the world, if provided
	if at_transform:
		if typeof(at_transform) == TYPE_TRANSFORM2D:
			player_ship.global_transform = at_transform
		else:
			print("POSITION PLAYER SHIP: expected Transform2D but got type ",typeof(at_transform))
	
	# Link Camera and Controller
	$Camera.follower = player_ship
	$Controller.target = player_ship
	
	# Print player's serial on their ship
	player_ship.set_serial( PILOT.current_serial )



func _ready():
	spawn_player_ship()
	connect("report_escape_velocity", $ShipUI, "_on_escape_velocity_changed")
	emit_signal( "report_escape_velocity", $World.escape_velocity )



# Emits once per second. Increments flight_time by one second's worth of game time
func _on_ClockTick_timeout():
	PILOT.add_flight_time( $ClockTick.wait_time )


func _on_player_ship_report_linear_velocity( vel ):
	match escaping:
		true:
			if vel < $World.escape_velocity:
				escaping = false
				emit_signal("player_ends_escape")
				$EscapeClock.stop()
		false:
			if vel >= $World.escape_velocity:
				escaping = true
				emit_signal("player_begins_escape")
				$EscapeClock.start()

func _on_EscapeClock_timeout():
	emit_signal("player_escapes")


func _on_FlightSim_player_escapes():
	get_parent().scene = GLOBAL.SYSTEM_MAP_SCENE
