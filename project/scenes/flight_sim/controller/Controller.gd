extends Node

var target

var rcs_input = Vector2()
var rot_input = 0.0

# RCS Input Softening factor. Lower values are softer
export(float) var soften = 5.0

func _process(delta):
	var DAMP_TOGGLE = Input.is_action_just_pressed("inertial_dampener_toggle")
	var DAMP_INCREASE = Input.is_action_just_pressed("inertial_dampener_increase_speed")
	var DAMP_DECREASE = Input.is_action_just_pressed("inertial_dampener_decrease_speed")
	var KILL_ROT = Input.is_action_just_pressed("kill_rotation_toggle")
	
	var RCS_F = Input.is_action_pressed("rcs_forward")
	var RCS_B = Input.is_action_pressed("rcs_back")
	var RCS_L = Input.is_action_pressed("rcs_left")
	var RCS_R = Input.is_action_pressed("rcs_right")
	
	var ROT_L = Input.is_action_pressed("rcs_rot_left")
	var ROT_R = Input.is_action_pressed("rcs_rot_right")
	
	var LX = Input.get_joy_axis(0,0)
	var LY = Input.get_joy_axis(0,1)
	var RX = Input.get_joy_axis(0,2)
	var RY = Input.get_joy_axis(0,3)
	
	# Process RCS linear input
	var input = Vector2( int(RCS_R)-int(RCS_L), int(RCS_F)-int(RCS_B) )
	# Override with joy input if present
	if abs(RX) > 0.1:
		input.x = RX
	if abs(RY) > 0.1:
		input.y = -RY
	
	# RCS Input X axis
	if abs(rcs_input.x) <= 1:
		if input.x != 0: #Increment input
			rcs_input.x += input.x * delta * soften
		else: #Decrement if no input
			rcs_input.x -= sign(rcs_input.x) * delta * soften * 2.5
			if abs(rcs_input.x) <= delta: #Cutoff if value is tiny
				rcs_input.x = 0
	else: #Clamp to 1/-1 if value is too high
		rcs_input.x = sign(rcs_input.x)
	
	#RCS Input Y axis
	if abs(rcs_input.y) <= 1:
		if input.y != 0: #Increment input
			rcs_input.y += input.y * delta * soften
		else: #Decrement if no input
			rcs_input.y -= sign(rcs_input.y) * delta * soften
			if abs(rcs_input.y) <= delta: #Cutoff if value is tiny
				rcs_input.y = 0
	else: #Clamp to 1/-1 if value is too high
		rcs_input.y = sign(rcs_input.y)
	
	# RCS rotation input
	var rinput = int(ROT_L) - int(ROT_R)
	# Override with joy input if present
	if abs(LX) > 0.1:
		rinput = LX
	
	if abs(rot_input) <= 1:
		if rinput != 0:
			rot_input += rinput * delta * soften
		else:
			rot_input -= sign(rot_input) * delta * soften
			if abs(rot_input) <= delta:
				rot_input = 0.0
	else:
		rot_input = sign(rot_input)




	# Apply controller inputs to target
	if target:
		if "rcs_input" in target:
			target.rcs_input = self.rcs_input
		if "rot_input" in target:
			target.rot_input = Vector2(self.rot_input,0)
		
		if DAMP_TOGGLE:
			if target.has_method("toggle_inertial_dampener"):
				target.toggle_inertial_dampener()
		var a = int(DAMP_INCREASE) - int(DAMP_DECREASE)
		if a !=0 and target.has_method("change_inertial_dampener_target_speed"):
			target.change_inertial_dampener_target_speed(a)
		
		if KILL_ROT:
			if target.has_method("toggle_kill_rot"):
				target.toggle_kill_rot()
		