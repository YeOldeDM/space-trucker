extends Node2D

"""
The in-flight player's POV.
Capable of following different nodes in the flight sim
"""

# The camera's target
var follower setget _set_follower

func _ready():
	set_process(false)

func _physics_process(delta):
	if !follower:
		# Just in case we lose her, turn this off
		set_process(false)
		return
	# Follow the follower
#	global_transform = follower.global_transform
	var target_pos = follower.position + follower.velocity - position
	var target_rot = follower.rotation - rotation
	
	position += target_pos * delta *2
	rotate(target_rot * delta)
	
	


func _set_follower( what ):
	follower = what
	# Disable follow behavior if target is null
	set_process(follower != null)