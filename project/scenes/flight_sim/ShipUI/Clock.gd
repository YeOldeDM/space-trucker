extends Panel

onready var day_of_week = $TimeDisplay/Box/Dow
onready var month = $TimeDisplay/Box/Month
onready var day = $TimeDisplay/Box/Day
onready var year = $TimeDisplay/Box/Year

onready var time = $TimeDisplay/Time

onready var blinker = $Blink

var s = 0

var weekdays = [
	null,
	"Mon",
	"Tue",
	"Wed",
	"Thu",
	"Fri",
	"Sat",
	"Sun",
]

var months = [
	null,
	"Jan",
	"Feb",
	"Mar",
	"Apr",
	"May",
	"Jun",
	"Jul",
	"Aug",
	"Sept",
	"Oct",
	"Nov",
	"Dec",
]

func tick( datetime ):
	day_of_week.text = weekdays[datetime.weekday]
	month.text = months[datetime.month]
	day.text = str( datetime.day ).pad_zeros(2)
	year.text = str( datetime.year - 25 )
	
	time.text = "%s:%s:%s" % [ 
			str( datetime.hour ).pad_zeros(2),
			str( datetime.minute ).pad_zeros(2),
			str( datetime.second ).pad_zeros(2),
			 ]
	
	blinker.play("blink")
	

func _process(delta):
	var dt = OS.get_datetime()
	if dt.second != s:
		s = dt.second
		tick(dt)
	
