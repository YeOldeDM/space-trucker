extends CanvasLayer

onready var escape_velocity_display = $Frame/VelocityDisplay/Display/Box/EscapeVelocity
onready var linear_velocity_display = $Frame/VelocityDisplay/Display/Box/LinearVelocity

onready var inertial_dampening_display = $Frame/VelocityDisplay/InertialDampening
onready var kill_rot_display = $Frame/RotationDisplay/RotDisplay/KillRot

onready var rotation_rate_display = $Frame/RotationDisplay/RotDisplay/RotRate
onready var heading_display = $Frame/RotationDisplay/Heading/Heading

onready var escape_display = $Frame/EscapeDisplay

const LED_GREEN = [Color("#0c2800"),Color("#53d64c")]




func num_to_digits( number ):
	# numbers come in as m/s. Convert to km/s
	var value = number / 10.0
	value = stepify(value, 0.1)
	return str(value*10).pad_zeros(clamp(str(value*10).length(), 4,5))

func draw_rotation_display( rate ):
	var d = $Frame/RotationDisplay/RotDisplay
	d.get_node("LeftArrow").default_color = LED_GREEN[ int(rate < -1.0) ]
	d.get_node("RightArrow").default_color = LED_GREEN[ int(rate > 1.0) ]
	d.get_node("Centered").default_color = LED_GREEN[ int(abs(rate) <= 1.0) ]
#	d.get_node("Centered2").default_color = LED_GREEN[ int(abs(rate) <= 1.0) ]

func _on_escape_velocity_changed( to ):
	escape_velocity_display.text = num_to_digits( to )

func _on_linear_velocity_changed( to ):
	linear_velocity_display.text = num_to_digits( to )

func _on_rotation_rate_changed( to ):
	# Convert absolute value to degrees
	var v = int( rad2deg( to ) ) 
	rotation_rate_display.text = str( abs(v) )
	draw_rotation_display( v )
	
	
func _on_heading_changed( to ):
	heading_display.text = str( int( to + 180 ) )



func _on_inertial_dampener_active_changed( to ):
	inertial_dampening_display.visible = to

func _on_inertial_dampener_target_speed_changed( to ):
	inertial_dampening_display.get_node("TargetSpeed").text = str( to ).pad_decimals(2)

func _on_kill_rotation_changed( to ):
	kill_rot_display.modulate = [Color(1,1,1), Color(0.2,0,0)][int(to)]


func _on_FlightSim_player_begins_escape():
	escape_display.show()


func _on_FlightSim_player_ends_escape():
	escape_display.hide()


func _on_DisplayTint_color_changed(color):
	for node in get_tree().get_nodes_in_group( "LED" ):
		node.modulate = color


func _ready():
	$Frame/DisplayTint.color = $Frame/DisplayTint.color
