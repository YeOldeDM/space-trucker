extends Node2D

export(NodePath) var camera

export(float) var offset = 0.0

export(int)var spread = 8000
export(float)var density = 4.0

var area_seed = 0

func _ready():
	make_stars()

func make_stars(area_seed=0):
	seed( area_seed )
	for i in int(spread*density):
		var s = $Star.duplicate()
		add_child(s)
		s.position = Vector2( -spread+randi()%spread*2, -spread+randi()%spread*2 )
		var sc = rand_range(0.3,1.3)
		var l = rand_range(0.8,1.0)
		s.scale = Vector2(sc,sc)
		s.modulate.a = l
	randomize()


func _physics_process(delta):
	if get_node(camera):
		var gp = get_node(camera).global_position
		global_transform.origin = gp * offset
#		$World.material.set_shader_param("x", gp.x / 100000.0 )
#		$World.material.set_shader_param("y", gp.y / 100000.0 )