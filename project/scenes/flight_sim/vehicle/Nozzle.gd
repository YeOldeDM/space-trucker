extends Node2D

var power = 0.0 setget _set_power

enum THRUST_DIRECTION {
	NONE,
	FORWARD,
	BACK,
	LEFT,
	RIGHT,
}

enum ROLL_DIRECTION {
	NONE,
	LEFT,
	RIGHT,
}

export(THRUST_DIRECTION) var nozzle_direction
export(ROLL_DIRECTION) var roll_direction






func _set_power( to ):
	power = clamp(to, 0.0, 1.0)
	scale.y = power
	scale.x = lerp(1.0,0.5,power)
	modulate.a = power