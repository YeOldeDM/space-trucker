extends KinematicBody2D

signal report_linear_velocity( to_velocity )
signal report_rotation_rate( rate )

signal report_heading( heading )

signal inertial_dampener_active_changed(to)
signal inertial_dampener_target_speed_changed(to)

signal kill_rotation_changed( to )

export(int) var mass = 25.0

export(int) var pri_thrust_impulse = 2500
export(int) var rcs_thrust_impulse = 4000

var velocity = Vector2(0,0)
var rot_velocity = 0.0

var primary_input = Vector2()
var rcs_input = Vector2()
var rot_input = Vector2()

var damping = 1.0

var inertial_dampener_active = false setget _set_inertial_dampener_active
var inertial_dampener_target_speed = 1.0 setget _set_inertial_dampener_target_speed

var kill_rotation = false setget _set_kill_rotation

var show_nozzles = false

var serial


func connect_to_ui( UI ):
	connect("report_linear_velocity", UI, "_on_linear_velocity_changed")
	connect("report_rotation_rate", UI, "_on_rotation_rate_changed")
	connect("report_heading", UI, "_on_heading_changed")
	connect("inertial_dampener_active_changed", UI, "_on_inertial_dampener_active_changed")
	connect("inertial_dampener_target_speed_changed", UI, "_on_inertial_dampener_target_speed_changed")
	connect("kill_rotation_changed", UI, "_on_kill_rotation_changed")

func toggle_inertial_dampener():
	set_inertial_dampener( !inertial_dampener_active )

func toggle_kill_rotation():
	self.kill_rotation = !self.kill_rotation

func set_inertial_dampener( state ):
	self.inertial_dampener_active = state
	if state:
		self.inertial_dampener_target_speed = get_linear_velocity()

func change_inertial_dampener_target_speed(by):
	if !inertial_dampener_active:
		return
	self.inertial_dampener_target_speed += by




func get_linear_velocity():
	return self.velocity.length()


func set_serial( ser ):
	self.serial = ser
	$Vis/SerialLabel.text = ser

func process_nozzles():
	for noz in $Vis/Nozzles.get_children():
		match noz.nozzle_direction:
			noz.THRUST_DIRECTION.FORWARD:
				noz.power = max(0, rcs_input.y)
			noz.THRUST_DIRECTION.BACK:
				noz.power = max(0, -rcs_input.y)
			noz.THRUST_DIRECTION.LEFT:
				noz.power = max(0, -rcs_input.x)
			noz.THRUST_DIRECTION.RIGHT:
				noz.power = max(0, rcs_input.x)
		match noz.roll_direction:
			noz.ROLL_DIRECTION.LEFT:
				noz.power += max(0, rot_input.x)
			noz.ROLL_DIRECTION.RIGHT:
				noz.power += max(0, -rot_input.x)


func _ready():
	self.show_nozzles = true

func _physics_process(delta):
	# Manipulate linear forces
	var force = Vector2()
	force.y -= ( (primary_input.y * pri_thrust_impulse) / self.mass ) * delta
	force.y -= ( (rcs_input.y * rcs_thrust_impulse) / self.mass ) * delta
	force.x += ( (rcs_input.x * rcs_thrust_impulse) / self.mass ) * delta
	
	# Apply forces along our local directional axes
	velocity += global_transform.x * force.x
	velocity += global_transform.y * force.y
	
	
	# Apply Cruise Control
	if inertial_dampener_active and inertial_dampener_target_speed < get_linear_velocity():
		if rcs_input.x == 0:
			velocity.x -= ( velocity.x / damping / mass ) * delta
			if abs(velocity.x) < 1.0:
				velocity.x = 0
		if rcs_input.y == 0:
			velocity.y -= ( velocity.y / damping / mass ) * delta
			if abs(velocity.y) < 1.0:
				velocity.y = 0
	
	
	
	# Manipulate rotational forces
	var rot_force = ( (rot_input.x * (self.rcs_thrust_impulse)/(self.mass*10)) / self.mass ) * delta
	
	self.rot_velocity -= rot_force
	
	# Anti-rotate if KillRot is on
	if kill_rotation and rot_force == 0:
		if abs(self.rot_velocity) < 0.00001:
			self.rot_velocity = 0.0
		else:
			self.rot_force -= ( rot_velocity / damping ) * delta
	
	
	# Move the vehicle
	if velocity.length() > 0:
		move_and_slide( velocity )
	if abs(rot_velocity) > 0:
		rotate( rot_velocity * delta )
		
	
	if self.show_nozzles:
		process_nozzles()
	
	# Project velocity vector line
	$Vector.rotation = -rotation
	$Vector.points[1] = velocity

func _on_VelocityPing_timeout():
	emit_signal( "report_linear_velocity", get_linear_velocity() )
	emit_signal( "report_rotation_rate", rot_velocity )
	emit_signal( "report_heading", rotation_degrees )
#	print(self.rcs_input)



func _set_inertial_dampener_active(what):
	inertial_dampener_active = what
	emit_signal( "inertial_dampener_active_changed", what )

func _set_inertial_dampener_target_speed(what):
	inertial_dampener_target_speed = what
	emit_signal( "inertial_dampener_target_speed_changed", what )


func _set_kill_rotation( what ):
	kill_rotation = what
	emit_signal( "kill_rotation_changed", what )


