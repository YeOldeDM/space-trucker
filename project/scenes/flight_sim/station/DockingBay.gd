extends Area2D

export(int) var bay_number = 1

var assigned_serial

# A ship enters the docking bay zone
func _on_DockingBay_body_entered(body):
	if 'serial' in body:
		prints( body.serial, "enters this dock" )

# A ship exits the docking bay zone
func _on_DockingBay_body_exited(body):
	if 'serial' in body:
		prints( body.serial, "exits this dock" )

func _ready():
	$BayNumber.text = str( self.bay_number )
