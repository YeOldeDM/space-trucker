extends Control

var can_skip = false

func next():
	SFX.play( SFX.BW_THEME )
	get_parent().scene = GLOBAL.PILOT_MANAGER_SCENE
	if get_parent():
		get_parent().get_node("UI/MenuButton").show()

func _ready():
	if get_parent():
		get_parent().get_node("UI/UIMenu").hide()




func _on_Timer_timeout():
	can_skip = true



func _input(ev):
	if 'pressed' in ev and ev.pressed:
		if can_skip:
			next()
