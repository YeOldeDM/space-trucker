extends Node

func get_distance( from, to ):
	var d1 = get_node( from ).distance
	var d2 = get_node( to ).distance
	
	return d2 - d1
