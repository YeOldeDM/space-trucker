extends CanvasLayer

signal dismissed()

func show_notification( text="Something happened! Click OK to continue.", ok_text="OK" ):
	$Dimmer/NotificationPanel/Box/NotificationText.text = text
	$Dimmer/NotificationPanel/Box/OK.text = ok_text
	$Dimmer.show()


func _ready():
	$Dimmer.hide()

func _on_OK_pressed():
	emit_signal("dismissed")
	$Dimmer.hide()
