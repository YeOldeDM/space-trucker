extends CanvasLayer




func _ready():
	$UIMenu.hide()



func _on_MenuButton_pressed():
	$UIMenu.visible = !$UIMenu.visible
	get_node("UIMenu/BG").flicker()


func _on_Resume_pressed():
	$UIMenu.hide()


func _on_Quit_pressed():
	get_tree().quit()
