extends Node

# ???
signal pilot_loaded()

export(PackedScene) var scene = null setget _set_scene

# Container for the instance of the `scene` when its set
var _current_scene





func _ready():
	# Stir up the randomizer first thing
	randomize()
	

	
	# Skip the intro scene if user settings demand it
	if CONFIG.current.get_value("misc","skip_intro"):
			self.scene = GLOBAL.PILOT_MANAGER_SCENE
	# Default to intro scene if nobody set export var
	elif !_current_scene:
		self.scene = GLOBAL.INTRO_SCENE


func _notification(what):
	if what == MainLoop.NOTIFICATION_WM_QUIT_REQUEST:
		# SAVE CURRENT PILOT
		PILOT.save_current_pilot()
		yield( PILOT, "saved_pilot_file" )
		# QUIT
		get_tree().quit()


# Universal Scene Setter#
# This is where the magic happens
func _set_scene( what ):
	if _current_scene != null:
		_current_scene.queue_free()
	scene = what
	_current_scene = scene.instance()
	add_child(_current_scene)





