shader_type canvas_item;

uniform float rot_speed = 256.0; // smaller values rotate faster
uniform float pi = 3.14159;

void fragment() {
    float x = (UV.x - 0.5) * 2.0;
    float y = (UV.y - 0.5)  * 2.0;
    float r = sqrt(x * x + y * y);
    
    float d = bool(r) ? asin(r)/r : 0.0;
    float x2 = d * x;
    float y2 = d * y;
    
    
    
    float x3 =  mod(x2 / (4.0 * pi) + 0.5 + TIME / rot_speed, 1.0);
    float y3 = y2 / (2.0 * pi) + 0.5;
    
    //simulated light vector
    vec3 cLight = normalize(vec3(-0.5, -0.5, 1.0));

    vec2 center = vec2(0.5, 0.5);
    float radius = 0.5;
    
    vec2 position = UV - center;
    
    float z = sqrt(radius * radius - position.x * position.x - position.y * position.y);
    vec3 normal = normalize(vec3(position.x, position.y, z));
    
    if (length(position) > radius) { //discard pixels outside the sphere
        discard;
    } else {
        float diffuse = max(0.0, dot(normal, cLight));
        vec4 sample = texture(TEXTURE,vec2(x3,y3)); //grab sphere-mapped pixel from texture
        COLOR = vec4(vec3(diffuse), 1.0) * sample;
    }
}