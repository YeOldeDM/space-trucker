shader_type canvas_item;

uniform sampler2D pri_base_pal;
uniform sampler2D sec_base_pal;


uniform sampler2D pri_swap_pal;
uniform sampler2D sec_swap_pal;


void fragment(){
	for (int i=0; i < 7; i++){
		if (texture(TEXTURE,UV) == texelFetch(pri_base_pal, ivec2(i, 0), 0)){
			COLOR = texelFetch(pri_swap_pal, ivec2(i, 0), 0);
			break;
		}
		if (texture(TEXTURE,UV) == texelFetch(sec_base_pal, ivec2(i, 0), 0)){
			COLOR = texelFetch(sec_swap_pal, ivec2(i, 0), 0);
			break;
		}
	COLOR = texture(TEXTURE,UV);
	}
}