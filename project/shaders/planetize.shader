shader_type canvas_item;

render_mode blend_mix;

uniform float speed = 0.01;


void fragment(){
	vec2 ruv = UV - vec2(0.5,0.5);
	vec2 dir = normalize(ruv);
	float len = length(ruv)/3.0;	
	
	len = pow(len*3.14, 3.14)*3.14;
	ruv = len*dir;
	
	vec4 col = texture(TEXTURE, ruv + vec2(TIME*speed, 0.5));
	col.a = 1.0-(len*3.14);
	COLOR = col;
	
}