extends Node2D

func make_star(star):
	var s = $_S.duplicate()
	s.position = star.pos
	s.scale *= star.lum * 0.05
	s.modulate = STARGEN.StellarColors[star.stellar_class]
	add_child(s)

func _ready():
	randomize()
	var galaxy = STARGEN.make_galaxy()
	for star in galaxy:
		make_star(star)
	
	
	$_S.queue_free()
	
