extends Node

const INTRO_SCENE = preload("res://scenes/intro/Intro.tscn")
const PILOT_MANAGER_SCENE = preload("res://scenes/pilot_manager/PilotManager.tscn")
const JOB_SELECT_SCENE = preload("res://scenes/job_select/JobSelect.tscn")
const FLIGHT_SIM_SCENE = preload("res://scenes/flight_sim/FlightSim.tscn")
const SYSTEM_MAP_SCENE = preload("res://scenes/system_map/SystemMap.tscn")

const TIME_SCALE = 20

const VERSION = {
	"major":	0,
	"minor":	0,
	"baby":		2,
}

func get_name_and_version():
	var name = ProjectSettings.get_setting("application/config/name")
	name += " v%d.%d.%d" % [VERSION.major,VERSION.minor,VERSION.baby]
	return name


func notify( text="Something happened! Click OK to continue.", ok_text="OK" ):
	var notify = $"/root/Game/Notification"
	if !notify:
		return
	notify.show_notification(text,ok_text)
	return notify


