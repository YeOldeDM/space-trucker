extends Node




var time= 0	# Artificial in-game unix time

var running = false setget _set_running

func _ready():
	self.running = self.running	# Bootstrap time tracking

func _process(delta):
	self.time += delta

func _set_running( what ):
	running = what
	set_process( what )





