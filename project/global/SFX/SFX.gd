extends Node

const BW_THEME = "res://assets/sfx/bw_jingle.wav"

func play( sfx_path ):
	var st = load(sfx_path)
	var pl = AudioStreamPlayer.new()
	add_child(pl)
	pl.stream = st
	pl.connect("finished", pl, "queue_free")
	pl.play()
