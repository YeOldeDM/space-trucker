extends Node

const CONFIG_PATH = "user://config.cfg"

const CONFIG_TEMPLATE = {
	"audio": {},
	"video": {
		"fullscreen": false,
		"window_width": 1600,
		"window_height": 900,
		"fullscreen_width": 1600,
		"fullscreen_height": 900,
	},
	"controls": {},
	"misc": {
		"skip_intro": false,
	},
}


var current = ConfigFile.new()


func make_new_config():
	for cat in CONFIG_TEMPLATE:
		for key in CONFIG_TEMPLATE[cat]:
			current.set_value( cat, key, CONFIG_TEMPLATE[cat][key] )


func save_config(config):
	config.save(CONFIG_PATH)

func load_config():
	self.current = ConfigFile.new()
	var loaded = self.current.load(CONFIG_PATH)
	print("!CONFIG LOADED!\n")


func init_config():
	assert self.current is ConfigFile
	OS.window_fullscreen = current.get_value("video","fullscreen")
	if OS.window_fullscreen:
		OS.window_size = Vector2( current.get_value("video","fullscreen_width"),\
								  current.get_value("video","fullscreen_height") )
	else:
		OS.window_size = Vector2( current.get_value("video","window_width"),\
								  current.get_value("video","window_height") )


func _ready():
	var file = File.new()
	if !file.file_exists(CONFIG_PATH):
		make_new_config()
		save_config(self.current)
	
	load_config()





