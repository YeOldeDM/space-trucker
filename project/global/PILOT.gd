extends Node

signal loaded_pilot_file()
signal unloaded_pilot_file()
signal saved_pilot_file()


const PILOT_FILE_TEMPLATE = {
	"name":	"",
	"biological_age":		16,
	"chronological_age":	16,
	"game_date":			0,
	"flight_time":			0,
	"play_time":			0,
	"creation_time":		0,
	"licence":				[],
}


const SERIAL_CHARS = "ABCFGHJKNPQRTUXYZ"

# Holds the current serial ID for the loaded pilot
var current_serial
# Holds the dictionary for the loaded pilot's file
var current_pilot_file


var play_time = 0.0

func get_game_date(t=0):
	return OS.get_datetime_from_unix_time( t )

func generate_unique_serial():
	var list = get_pilots_list()
	var s = generate_serial()
	while s in list:
		s = generate_serial()
	return s



func generate_serial():
	var s = ""
	for i in 5:
		if randi()%4 < 3:
			s += str( randi()%10 )
		else:
			s += SERIAL_CHARS[randi()%SERIAL_CHARS.length()]
	
	s += "-"
	
	for i in 3:
		if randi()%4 > 1:
			s += str( randi()%10 )
		else:
			s += SERIAL_CHARS[randi()%SERIAL_CHARS.length()]
	
	return s




func make_pilot_file_template():
	var data = {}
	for key in PILOT_FILE_TEMPLATE:
		data[key] = PILOT_FILE_TEMPLATE[key]
	return data





func _ready():
	var dir = Directory.new()
	if !dir.dir_exists("user://pilots/"):
		_make_pilots_file()
		OS.alert("Welcome to the game! A pilots folder has been created in your user directory!", "Don't Be Scared!")
	else:
		print("!PILOT DIRECTORY FOUND!\n")

	


func _process(delta):
	play_time += delta


func get_play_time():
	var time = self.play_time
	if self.current_pilot_file:
		if "play_time" in self.current_pilot_file:
			time += self.current_pilot_file.play_time
	return time


func add_flight_time( real_time ):
	# Convert to game time
	var time = real_time * GLOBAL.TIME_SCALE
	# Add the time to the current file data
	if self.current_pilot_file:
		self.current_pilot_file.flight_time += time


func _make_pilots_file():
	var dir = Directory.new()
	dir.open("user://")
	dir.make_dir("pilots")



func load_pilot( serial ):
	var data = get_pilot_file( serial )
	
	assert typeof(data) == TYPE_DICTIONARY
	
	self.current_serial = serial
	self.current_pilot_file = data
	
	emit_signal("loaded_pilot_file")



func unload_pilot(save_first=true):
	if save_first:
		save_current_pilot()
		yield(self, "saved_pilot_file")
	self.current_serial = null
	self.current_pilot_file = null
	emit_signal("unloaded_pilot_file")


func create_pilot( serial, name="Ham Soro", age=16 ):
	var file = File.new()
	var opened = file.open( "user://pilots/%s" % serial, file.WRITE )
	
	# Grab a duplicate of our pilot file template
	var data = make_pilot_file_template()
	
	# Set data from arguments
	data.name = name
	data.biological_age = age
	data.chronological_age = age
	# Set data from other data
	data.version = GLOBAL.VERSION
	data.creation_time = OS.get_unix_time()
	# Save the new data to the new file
	file.store_line( to_json( data ) )
	file.close()
	




func get_pilot_file( serial ):
	var file = File.new()
	var opened = file.open( "user://pilots/%s" % serial, file.READ )
	if !opened == OK:
		return opened
	var data = parse_json( file.get_line() )
	
	file.close()
	return data


func save_pilot_file( serial, data ):
	var file = File.new()
	var opened = file.open( "user://pilots/%s" % serial, file.WRITE )
	
	# Set the total play time to data
	data.version = GLOBAL.VERSION
	data.play_time = get_play_time()
	
	file.store_line( to_json( data ) )
	file.close()

func save_current_pilot():
	if self.current_serial and self.current_pilot_file:
		save_pilot_file( self.current_serial, self.current_pilot_file )
	emit_signal("saved_pilot_file")

# UNTESTED!!!
func delete_pilot_file( serial ):
	var dir = Directory.new()
	var deleted = dir.remove( "user://pilots/%s" % serial )
	if deleted == OK:
		print( "!DELETED PILOT FILE %s!\n" % serial )
	else:
		print( "something went wrong!" )
		

	


func get_pilots_list():
	var dir = Directory.new()
	var opened = dir.open("user://pilots")
	var list = []
	dir.list_dir_begin(true,true)
	var file = dir.get_next()
	while file != "":
		list.append( file )
		file = dir.get_next()
	
	return list
	